import React, { useRef, useState } from 'react';
import './css/style.css';

import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

import { useAuthState } from 'react-firebase-hooks/auth';
import { useCollectionData } from 'react-firebase-hooks/firestore';

firebase.initializeApp({
	apiKey: "AIzaSyBLCtdJ9-S2MdsMPbA9nShmqMCYUoPiCIk",
	authDomain: "superchat-aa314.firebaseapp.com",
	projectId: "superchat-aa314",
	storageBucket: "superchat-aa314.appspot.com",
	messagingSenderId: "1000584908825",
	appId: "1:1000584908825:web:b1f0363cd2339857575a88"
})

const auth = firebase.auth();
const firestore = firebase.firestore();

function App() {
	const [user] = useAuthState(auth);

	return (
		<div className="App">
			<header>
				<h1>Superchat</h1>
				<SignOut />
			</header>

			<section>
				{user ? <Chat /> : <SignIn />}
			</section>
		</div>
	);
}

function SignIn() {
	const signInWithGoogle = () => {
		const provider = new firebase.auth.GoogleAuthProvider();
		auth.signInWithPopup(provider);
	}

	return (
		<button className='sign-in' onClick={signInWithGoogle}>Zaloguj się z Google</button>
	)
}

function SignOut() {
	return auth.currentUser && (
		<button className='sign-out' onClick={() => auth.signOut()}>Wyloguj</button>
	)
}

function Chat() {
	const dummy = useRef();
	const messagesRef = firestore.collection('messages');
	const query = messagesRef.orderBy('createdAt').limit(25);

	const [messages] = useCollectionData(query, { idField: 'id' });
	const [formValue, setFormValue] = useState('');

	const sendMessage = async (e) => {
		e.preventDefault();

		const { uid, photoURL } = auth.currentUser;

		await messagesRef.add({
			text: formValue,
			createdAt: firebase.firestore.FieldValue.serverTimestamp(),
			uid,
			photoURL
		});

		setFormValue('');

		dummy.current.scrollIntoView({ behaviour: 'smooth' });
	}

	return (
		<>
			<main>
				<div>
					{messages && messages.map(msg => <ChatMessage key={msg.id} message={msg} />)}

					<div ref={dummy}></div>
				</div>
			</main>

			<form onSubmit={sendMessage}>
				<input value={formValue} onChange={e => setFormValue(e.target.value)} placeholder='Wiadomość...' />
				<button type='submit' disabled={!formValue}>Wyślij</button>
			</form>
		</>
	);
}

function ChatMessage(props) {
	const { text, uid, photoURL } = props.message;

	const messageClass = uid === auth.currentUser.uid ? 'sent' : 'received';

	return (
		<div className={`message ${messageClass}`}>
			<img src={photoURL} />
			<p>{text}</p>
		</div>
	);
}

export default App;
